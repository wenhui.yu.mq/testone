﻿using Autofac;
using System.Linq;
using System.Reflection;


namespace Infrastructure
{
    public static class ServiceConatinerExtensions
    {
        public static ContainerBuilder AddRegistriesForRepository(this ContainerBuilder builder)
        {

            var repo_interface = Assembly.Load("Repository.interfaces");
            var repo = Assembly.Load("Repository.DefaultImplementation");
            builder.RegisterAssemblyTypes(repo, repo_interface).Where(x => x.Name.EndsWith("Repository"))
                                                               .AsImplementedInterfaces()
                                                               .PropertiesAutowired();
            return builder;
        }
    }
}
