﻿using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;

namespace Infrastructure
{
    public static class SwaggerSettings
    {
        public static IServiceCollection AddRegisterSwaggerService(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "未来科技研究所",
                    Description = "欢迎来到未来科技研究所 .net core",
                    Contact = new Contact
                    {
                        Name = "wenhui.yu",
                        Email = "15697400137@163.com",
                        Url = string.Empty
                    },
                    License = new License
                    {
                        Name = "is ok",
                        Url = string.Empty
                    }
                });

                var basePath = Path.Combine(Directory.GetCurrentDirectory(), "Swagger.xml");
                options.IncludeXmlComments(basePath);
            });
            return services;
        }
    }
}
