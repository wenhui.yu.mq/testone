﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Repository.interfaces;

namespace Repository.DefaultImplementation
{
    public class RepositoryBse<T> : IRepositoryBse<T>
                                    where T : class
    {
        private static readonly DBContext _dBContext =new DBContext() ;

        static RepositoryBse()
        {
            
        }

        public virtual T Create(T entity)
        {
            _dBContext.Set<T>().Add(entity);
            _dBContext.SaveChanges();
            return entity;
        }

        public virtual async Task<T> CreateAsync(T entity)
        {
            await _dBContext.Set<T>().AddAsync(entity);
            await _dBContext.SaveChangesAsync();
            return entity;
        }

        public virtual bool Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public virtual Task<bool> DeleteAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public virtual T Get<K>(K primary)
        {
            throw new NotImplementedException();
        }

        public virtual async Task<T> GetAsync<K>(K primary)
        {
            var entity = await _dBContext.Set<T>().FindAsync(primary);
            return entity;
        }

        public virtual T GetList(Func<T, bool> func)
        {
            throw new NotImplementedException();
        }

        public virtual Task<T> GetListAsync(Func<T, bool> func)
        {
            throw new NotImplementedException();
        }
    }
}
