﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.interfaces
{
    public interface IRepositoryBse<T>
    {
        Task<T> GetAsync<K>(K primary);
        T Get<K>(K primary);
        Task<T> GetListAsync(Func<T, bool> func);
        T GetList(Func<T, bool> func);

        Task<T> CreateAsync(T entity);
        T Create(T entity);

        bool Delete(T entity);
        Task<bool> DeleteAsync(T entity);
    }
}
