﻿using Microsoft.AspNetCore.Mvc;
using Repository.DefaultImplementation;
using Repository.interfaces;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IUserRepository _userRepository;
        public ProductController(IUserRepository userRepositoryt)
        {
            _userRepository = userRepositoryt;
        }

        [HttpGet("List")]
        public async Task<IActionResult> List()
        {
            var repo = (UserRepository)_userRepository;
            var result = new { Id = "111" };
            return new JsonResult(result);
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var repo = (UserRepository)_userRepository;
            var result = await repo.GetAsync(id);
            return new JsonResult(result);
        }
    }
}

